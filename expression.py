# -*- python3 -*-

u"""
Exports classes for defining Parsing Expressions.

We define a parsing expression inductively. A parsing expression is one
of

  - `ε`, the empty string; or
  - `a`, any terminal where `a` is in `V_T`; or
  - `A`, any nonterminal where `A` is in `V_N`; or
  - `e1 e2 ... eN`, a sequence, if `e1`, `e2`, ..., `eN` are
    expressions; or
  - `e_1 / e_2 / ... / eN`, prioritized choice, if `e1`, ..., `eN` are
    expressions; or
  - `e*`, zero-or-more repetitions, if `e` is an expression; or
  - `!e`, a not-predicate, if `e` is an expression

Some sugar is also defined:

  - `&e` is sugar for `!(!e)`.
  - `e?` is sugar for `e / ε`
  - `e+` is sugar for `ee*`
  - "..." or '...' is sugar for a sequence of literal characters.
  - [...] is sugar for a character class, possibly specified as ranges.
  - . is sugar for the class of all terminals in `V_T`.

The objects corresponding to the basic expressions are:

  - Empty()
  - Terminal(a)
  - Nonterminal(A)
  - Sequence([e1, e2, ..., eN])
  - Choose([e1, e2, ..., eN])
  - Repeat(e)
  - Not(e)

and to the sugarred expressions:

  - And(e)
  - Option(e)
  - OneOrMore(e)
  - Literal(string)
  - Class(string)
  - Dot()

The classes exported by `expression` are

  - And: Represents an and predicate
  - Base: Base class of all expression types
  - Choose: Represents a prioritized choice
  - Class: Represents a character class
  - Dot: Represents the character class for `V_T`
  - Drop: Represents a node that is not kept in the syntax tree
  - Empty: Represents the empty expression
  - HalfDrop: Represents a dropped node with significant matches
  - Literal: Represents a string literal
  - Nonterminal: Represents a single nonterminal symbol
  - Not: Represents a not predicate
  - OneOrMore: Represents a one-or-more repetition
  - Option: Represents an option expression
  - Repeat: Represents a zero-or-more repetition
  - Sequence: Represents the juxtaposition of expressions
  - Symbol: Base class of all symbol type
  - Terminal: Represents a terminal symbol

"""

import abc
import itertools

import result
import parse_node
import string

# TODO: For specialization of match, ensure that the most sensible error
# position is reported. E.g., for literals, the position should be the
# start of the literal, while, as a sub-error, the position of the
# actual terminal mismatch is recorded.

# TODO: Add customization points in the Base hierarchy. Currently, I'm
# thinking of a `on_failure` method that will allow, for example, the
# Class class to provide a more sane Failure object then the default
# Choose class will. Other extension points may be possible. It may even
# be that much of the `match` method can be factored into extension
# points.


class Base(object, metaclass=abc.ABCMeta):

    """
    The base class for all expression types.

    Public interface:

      - match

    Additionally, subclasses must implement the match method.

    """

    def __init__(self):
        pass

    @abc.abstractmethod
    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        raise NotImplementedError

    def flatten(self, node):
        """
        Attempt to flatten the PEST `node`.

        Any "helper" node, such as a `Sequence`, is flattened into its
        children. On the other hand, concrete nodes, such as `Terminal`,
        `Nonterminal` and `Literal` nodes, are structurally important
        and/or logically indivisible, and thus flatten into themselves.

        Return type: list of parse_node.ParseNode

        """
        return node.children


class Empty(Base):

    """Represents the empty expression."""

    def __init__(self):
        Base.__init__(self)

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        # 1. Empty
        node = parse_node.ParseNode(expression=self,
                                    input_string=parser.input,
                                    start=index,
                                    end=index,
                                    sub_matches=[],
                                    children=[])
        return result.Success(node)


class Sequence(Base):

    """Represents a sequence of parsing expressions."""

    def __init__(self, children):
        Base.__init__(self)

        self.children = list(children)

        for i, child in enumerate(self.children):
            assert (isinstance(child, Base) or isinstance(child, str)), \
                '{}'.format(child)
            if isinstance(child, str):
                self.children[i] = Nonterminal(child)

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        child_nodes = [None] * len(self.children)
        running_index = index
        for i, child in enumerate(self.children):
            match_result = child.match(parser, running_index)
            if match_result.is_failure:
                # 6 & 7. Sequence (Failure cases)
                return match_result

            child_nodes[i] = match_result.node
            running_index = match_result.node.end

        # 5. Sequence (Success case)
        matched = parser.sub_input(index, running_index)
        children = list(itertools.chain.from_iterable(
            node.flatten() for node in child_nodes
        ))
        sub_matches = itertools.chain.from_iterable(
            child.sub_matches for child in child_nodes
        )
        node = parse_node.ParseNode(expression=self,
                                    input_string=parser.input,
                                    start=index,
                                    end=index + len(matched),
                                    sub_matches=sub_matches,
                                    children=children)
        return result.Success(node)


class Choose(Base):

    """Represents a prioritized choice of parsing expressions."""

    def __init__(self, children):
        Base.__init__(self)

        self.children = list(children)

        for i, child in enumerate(self.children):
            assert (isinstance(child, Base) or isinstance(child, str)), \
                '{}'.format(child)
            if isinstance(child, str):
                self.children[i] = Nonterminal(child)

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        node = None
        failures = [None] * len(self.children)
        for i, child in enumerate(self.children):
            match_result = child.match(parser, index)
            if match_result.is_success:
                # 8. Alternative (Success cases)
                children = match_result.node.flatten()
                sub_matches = match_result.node.sub_matches
                node = parse_node.ParseNode(expression=self,
                                            input_string=parser.input,
                                            start=match_result.node.start,
                                            end=match_result.node.end,
                                            sub_matches=sub_matches,
                                            children=children)
                return result.Success(node)

            failures[i] = match_result.errors

        # 9. Alternative (Failure case)
        return result.Failure(itertools.chain.from_iterable(failures))


class Repeat(Base):

    """Represents a zero-or-more repetition of a parsing expression."""

    def __init__(self, expression):
        Base.__init__(self)

        assert (isinstance(expression, Base) or isinstance(expression, str)), \
            '{}: {}'.format(type(expression), expression)

        if isinstance(expression, str):
            expression = Nonterminal(expression)

        self.expression = expression

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        child_nodes = []
        running_index = index
        while True:
            match_result = self.expression.match(parser, running_index)
            if match_result.is_failure:
                break
            child_nodes.append(match_result.node)
            running_index = match_result.node.end

        children = list(itertools.chain.from_iterable(
            node.flatten() for node in child_nodes
        ))
        sub_matches = itertools.chain.from_iterable(
            child.sub_matches for child in child_nodes
        )
        node = parse_node.ParseNode(expression=self,
                                    input_string=parser.input,
                                    start=index,
                                    end=running_index,
                                    sub_matches=sub_matches,
                                    children=children)
        return result.Success(node)


class Not(Base):

    """Represents a not predicate of a parsing expression."""

    def __init__(self, expression):
        Base.__init__(self)

        assert (isinstance(expression, Base) or isinstance(expression, str)), \
            '{}'.format(expression)

        if isinstance(expression, str):
            expression = Nonterminal(expression)

        self.expression = expression

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        sub_match = self.expression.match(parser, index)

        if sub_match.is_success:
            # 12. Not-predicate (case 1)
            # TODO: change error to depend on sub_match's value.
            return result.Failure([
                result.ParseError('unexpected {}'.format(self.expression),
                                  parser.position_at_index(index),
                                  False)
            ])

        # 13. Not-predicate (case 2)
        node = parse_node.ParseNode(expression=self,
                                    input_string=parser.input,
                                    start=index,
                                    end=index,
                                    sub_matches=[],
                                    children=[])
        return result.Success(node)


# Note: with our new methodology, a drop expression's behaviour is
# implicit, not explicit. Any expression which doesn't return children
# during a match is equivalent to a drop node.
class HalfDrop(Base):

    """Represents a parsing expression whose children are dropped."""

    def __init__(self, expression):
        Base.__init__(self)

        assert isinstance(expression, Base) or isinstance(expression, str), \
            '{}'.format(expression)

        if isinstance(expression, str):
            expression = Nonterminal(expression)

        self.expression = expression

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        sub_match = self.expression.match(parser, index)

        if sub_match.is_success:
            # A.3. Half-drop node success case
            node = parse_node.ParseNode(expression=self,
                                        input_string=parser.input,
                                        start=sub_match.node.start,
                                        end=sub_match.node.end,
                                        sub_matches=sub_match.node.sub_matches,
                                        children=[])
            return result.Success(node)

        # A.4. Half-drop node failure case
        return sub_match


# Note: with our new methodology, a drop expression's behaviour is
# implicit, not explicit. Any expression which doesn't return children
# during a match is equivalent to a drop node.
class Drop(Base):

    """Represents a dropped parsing expression."""

    def __init__(self, expression):
        Base.__init__(self)

        assert isinstance(expression, Base) or isinstance(expression, str), \
            '{}'.format(expression)

        if isinstance(expression, str):
            expression = Nonterminal(expression)

        self.expression = expression

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        sub_match = self.expression.match(parser, index)
        if sub_match.is_success:
            # A.1. Drop node success case
            node = parse_node.ParseNode(expression=self,
                                        input_string=parser.input,
                                        start=sub_match.node.start,
                                        end=sub_match.node.end,
                                        sub_matches=[],
                                        children=[])
            return result.Success(node)
        else:
            # A.2. Drop node fail case
            return sub_match


class Symbol(metaclass=abc.ABCMeta):

    """
    Base class for all symbols.

    `Symbol` Implements a disjoint union of terminals and
    nonterminals by means of testing methods and a generic decoherence
    method `apply`.

    Public interface:

      - is_terminal
      - is_nonterminal
      - apply
      - __eq__
      - __ne__

    Subclasses must also implement the following methods

      - apply

    """

    def __init__(self, name):
        """
        Initialize a `Symbol` instance.

        Set `self.name` to the given argument value. `name` should
        somehow identify the symbol. Usually, `name` is of `str` type or
        similar, to parallel terminals and nonterminals in most formal
        grammars. However, this is neither necessary nor specifically
        encouraged.

        Parameters:

          - name: identifying name for the symbol

        """
        self.__name = name

    @property
    def name(self):
        """
        Retrieve the name of `self`.

        Return type: unspecified

        """
        return self.__name

    def is_terminal(self):
        """
        Test whether `self` is a terminal symbol.

        Return type: bool

        """
        return self.apply(lambda _: True, lambda _: False)

    def is_nonterminal(self):
        """
        Test whether `self` is a nonterminal symbol.

        Return type: bool

        """
        return self.apply(lambda _: False, lambda _: True)

    @abc.abstractmethod
    def apply(self, if_terminal, if_nonterminal):
        """
        Perform decoherence on the `Symbol`.

        Apply `if_terminal` to `self` if `self` represents a terminal
        symbol. Otherwise, apply `if_nonterminal` to `self` if `self` is
        a nonterminal symbol. Return the result of applying `if_terminal`
        or `if_nonterminal`.

        Note:

          Exactly one of `if_terminal` and `if_nonterminal` is
          executed.

        Return type: the return value of `if_terminal` or
          `if_nonterminal`, whichever was executed.

        """
        raise NotImplementedError

    def __eq__(self, other):
        """
        Check whether `self` and `other` represent the same symbol.

        Return type: bool

        """
        return type(self) == type(other) and self.__name == other.name

    def __ne__(self, other):
        """
        Check if `self` and `other` do not represent the same symbol.

        Return type: bool

        """
        return not (self == other)

    def __hash__(self):
        return hash(self.__name)


class Terminal(Symbol, Base):

    """Represents a terminal symbol in a formal grammar."""

    def __init__(self, name):
        Symbol.__init__(self, name)
        Base.__init__(self)

    def apply(self, if_terminal, if_nonterminal):
        """
        Execute `if_terminal` on `self`.

        Return type: return type of `if_terminal`.

        """
        return if_terminal(self)

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """
        next_symbol = parser.at_index(index)
        if self == next_symbol:
            # 2. Terminal (success case)
            node = parse_node.ParseNode(expression=self,
                                        input_string=parser.input,
                                        start=index,
                                        end=index+1,
                                        sub_matches=[self.name],
                                        children=[])
            return result.Success(node)
        else:
            # 3. Terminal (failure case)
            return result.Failure([
                result.ParseError('expected {}'.format(self),
                                  parser.position_at_index(index),
                                  False)
            ])

    def flatten(self, node):
        """Return the singleton list of `self`."""
        return [node]

    def __str__(self):
        return repr(self.name)

    def __repr__(self):
        return self.__str__()


class Nonterminal(Symbol, Base):

    """Represents a nonterminal symbol in a formal grammar."""

    def __init__(self, name):
        Symbol.__init__(self, name)
        Base.__init__(self)

    def apply(self, if_terminal, if_nonterminal):
        """
        Execute `if_nonterminal` on `self`.

        Return type: return type of `if_nonterminal`.

        """
        return if_nonterminal(self)

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """

        def matcher():
            # 4. Nonterminal
            rhs_match = parser.grammar[self].match(parser, index)
            # Failure case
            if rhs_match.is_failure:
                # Only replace the failure error if another nonterminal
                # does not already completely "own" the failure.
                return result.Failure([
                    msg if msg.is_complete
                    else result.ParseError('expected {}'.format(self),
                                           parser.position_at_index(index),
                                           True,
                                           sub_error=msg)
                    for msg in rhs_match.errors
                ])

            # Success case
            children = rhs_match.node.flatten()
            node = parse_node.ParseNode(expression=self,
                                        input_string=parser.input,
                                        start=rhs_match.node.start,
                                        end=rhs_match.node.end,
                                        sub_matches=rhs_match.node.sub_matches,
                                        children=children)
            return result.Success(node)

        return parser.match(self, index, matcher)

    def flatten(self, node):
        """Return the singleton list of `self`."""

        return [node]

    def __str__(self):
        return '<{}>'.format(self.name.__str__())

    def __repr__(self):
        return self.__str__()


class Option(Choose):

    """Represents a zero-or-one repetition of a parsing expression."""

    def __init__(self, expression):
        Choose.__init__(self, [expression, Empty()])


class OneOrMore(Sequence):

    """Represents a one-or-more repetition of a parsing expression."""

    def __init__(self, expression):
        Sequence.__init__(self, [expression, Repeat(expression)])


class And(Not):

    """Represents an and predicate of a parsing expression."""

    def __init__(self, expression):
        Not.__init__(self, Not(expression))

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """

        match_result = Not.match(self, parser, index)
        if match_result.is_success:
            return match_result

        return result.Failure([
            result.ParseError('expected {}'.format(self.expression.expression),
                              parser.position_at_index(index),
                              False)
        ])


class Literal(Sequence):

    """Represents a string literal parsing expression."""

    def __init__(self, string):
        self.string = str(string)
        Sequence.__init__(self, (Terminal(char) for char in string))

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """

        match_result = Sequence.match(self, parser, index)
        if match_result.is_success:
            node = parse_node.ParseNode(expression=self,
                                        input_string=parser.input,
                                        start=match_result.node.start,
                                        end=match_result.node.end,
                                        sub_matches=[self.string],
                                        children=[])
            return result.Success(node)

        # We should have gotten a singular error message, for the
        # mismatched terminal symbol.
        return result.Failure([
            result.ParseError('expected {}'.format(repr(self.string)),
                              parser.position_at_index(index),
                              True,
                              sub_error=match_result.errors[0])])

    def flatten(self, node):
        """Return the singleton list of `self`."""

        return [node]

    def __str__(self):
        return repr(self.string)


class Class(Choose):

    """Represents a character class parsing expression."""

    def __init__(self, chars):
        # TODO: Allow string to have ranges.
        self.chars = chars

        Choose.__init__(self, (Terminal(char) for char in chars))

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """

        match_result = Choose.match(self, parser, index)
        if match_result.is_success:
            return match_result

        return result.Failure([
            result.ParseError('expected one of {}'.format(repr(self.chars)),
                              parser.position_at_index(index),
                              False)])


class Dot(Class):

    """Represents the character class for all of `V_T`."""

    def __init__(self):
        Class.__init__(self, string.printable)

    def match(self, parser, index):
        """
        Attempt to match the input with the given expression.

        Return a `result.Success` value if the parser can handle the
        expression at `index`. Otherwise, return `result.Failure`.

        """

        match_result = Class.match(self, parser, index)
        if match_result.is_success:
            return match_result

        return result.Failure([
            result.ParseError('expected character',
                              parser.position_at_index(index),
                              False)])
