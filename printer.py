"""
Provides a pretty printer for syntax trees of a PEG grammar.

Really, this module is does not provide a "printer" per se, but rather
a node-to-string conversion function.

This module exports the following functions:

  - pretty_print: Converts a given node to a pretty string.

"""

import expression

import string


def pretty_print(node):
    """
    Pretty print a syntax tree of a PEG grammar to a string.

    Parameters:

      - node: The root of the syntax tree which is to be printed

    Return value: a string result of pretty printing the node.

    """

    def fail(_):
        assert False, 'Node is not from a PEST'

    assert node.expression.is_nonterminal, 'Node is not from a PEST'

    # cool "switch" statement!
    return {'Grammar': _handle_grammar,
            'Definition': _handle_definition,
            'Expression': _handle_expression,
            'Sequence': _handle_sequence,
            'Prefix': _handle_prefix,
            'Suffix': _handle_suffix,
            'Primary': _handle_primary,
            'Identifier': _handle_identifier,
            'Literal': _handle_literal,
            'Class': _handle_class,
            'Range': _handle_range,
            'Char': _handle_char,
            'DOT': _handle_dot
            }.get(node.expression.name, fail)(node)


def _handle_grammar(node):
    # first definition
    # Structure: Grammar-Definition-Identifier
    start_node = node.children[0].children[0]
    # first definition's identifier
    start = _handle_identifier(start_node)

    definition_result = (_handle_definition(child) for child in node.children)
    return 'grammar.Grammar({}, [\n    '.format(start) \
        + ',\n    '.join(definition_result) \
        + '\n])'


def _handle_definition(node):
    identifier_result = _handle_identifier(node.children[0])
    expression_result = _handle_expression(node.children[1])
    return '({}, {})'.format(identifier_result, expression_result)


def _handle_expression(node):
    if len(node.children) == 1:
        return _handle_sequence(node.children[0])

    sequence_result = (_handle_sequence(child) for child in node.children)
    return 'expression.Choose([' + ', '.join(sequence_result) + '])'


def _handle_sequence(node):
    if len(node.children) == 1:
        return _handle_prefix(node.children[0])

    prefix_result = (_handle_prefix(child) for child in node.children)
    return 'expression.Sequence([' + ', '.join(prefix_result) + '])'


def _handle_prefix(node):
    if len(node.children) == 2:  # i.e., we actually have a prefix
        if node.children[0].expression == expression.Nonterminal('AND'):
            format_str = 'expression.And({})'
        elif node.children[0].expression == expression.Nonterminal('NOT'):
            format_str = 'expression.Not({})'
        elif node.children[0].expression == expression.Nonterminal('DROP'):
            format_str = 'expression.Drop({})'
        elif node.children[0].expression == expression.Nonterminal('HALFDROP'):
            format_str = 'expression.HalfDrop({})'
        else:
            assert False
        return format_str.format(_handle_suffix(node.children[1]))

    # no prefix
    return str(_handle_suffix(node.children[0]))


def _handle_suffix(node):
    if len(node.children) == 2:  # i.e., we actually have a suffix
        if node.children[1].expression == expression.Nonterminal('QUESTION'):
            format_str = 'expression.Option({})'
        elif node.children[1].expression == expression.Nonterminal('STAR'):
            format_str = 'expression.Repeat({})'
        elif node.children[1].expression == expression.Nonterminal('PLUS'):
            format_str = 'expression.OneOrMore({})'
        else:
            assert False
    else:
        format_str = '{}'
    return format_str.format(_handle_primary(node.children[0]))


def _handle_primary(node):
    child = node.children[0]
    if child.expression == expression.Nonterminal('Identifier'):
        return _handle_identifier(child)
    elif child.expression == expression.Nonterminal('Expression'):
        return '{}'.format(_handle_expression(child))
    elif child.expression == expression.Nonterminal('Literal'):
        return _handle_literal(child)
    elif child.expression == expression.Nonterminal('Class'):
        return _handle_class(child)
    elif child.expression == expression.Nonterminal('DOT'):
        return _handle_dot(child)
    else:
        assert False, str(child.expression)


def _handle_identifier(node):
    return repr(''.join(node.sub_matches))


def _handle_literal(node):
    char_result = (_handle_char(child) for child in node.children)
    return 'expression.Literal(' + repr(''.join(char_result)) + ')'


def _handle_class(node):
    range_result = (_handle_range(child) for child in node.children)
    return 'expression.Class(' + repr(''.join(range_result)) + ')'


def _handle_range(node):
    char1 = _handle_char(node.children[0])
    if len(node.children) == 2:  # i.e., we have a proper range
        char2 = _handle_char(node.children[1])
        index1 = str.find(string.ascii_lowercase, char1)
        if index1 >= 0:
            # lowercase range
            index2 = str.find(string.ascii_lowercase, char2)
            assert index2 >= 0
            chars = string.ascii_lowercase[index1: index2 + 1]
        else:
            index1 = str.find(string.ascii_uppercase, char1)
            if index1 >= 0:
                # uppercase range
                index2 = str.find(string.ascii_uppercase, char2)
                assert index2 >= 0
                chars = string.ascii_uppercase[index1: index2 + 1]
            else:
                index1 = str.find(string.digits, char1)
                if index1 >= 0:
                    # digit range
                    index2 = str.find(string.digits, char2)
                    assert index2 >= 0
                    chars = string.digits[index1: index2 + 1]
                else:
                    assert False, '{}'.format(node)

        return chars
    else:  # i.e., we have a single literal character
        return _handle_char(node.children[0])


def _handle_char(node):
    if len(node.children) == 2:  # i.e., we have an escape sequence
        assert node.children[0].expression.string == '\\'
        other = node.children[1].expression.name
        if other == 'n':
            return '\n'
        elif other == 'r':
            return '\r'
        elif other == 't':
            return '\t'
        else:
            return other
    else:
        return str(node.full_match)


def _handle_dot(node):
    return 'expression.Dot()'
