# -*- python3 -*-

"""
This module provides classes representing result from PEG recognitions.

This module provides the functionality needed for recognition code to
represent successful and unsuccessful recognition during a parse. It
also attaches error messages to failures so that user-friendly messages
may be posted. Finally, a function is provided to nicely print error
messages, complete with position information.

This module exports the following classes:

  - ParseError: Represents an error message
  - Result: The base class of all result types
  - Success: Represents a successful parse
  - Failure: Represents an unsuccessful parse

The following function are also exported:

  - print_error: print a ParseError to standard output

"""


class ParseError:

    """
    Represents an error message produced by a parser.

    In addition to a textual string message, a `ParseError` maintains
    various metadata. This includes the position at which the parse
    error occured, whether the error is a maximal error (i.e.,
    represents a fully formed error message) or still needs to be
    wrapped in a more informative error message. It also possibly
    maintains a child error.

    """

    def __init__(self, message, position, complete, sub_error=None):
        self.__message = message
        self.__sub_error = sub_error
        self.__position = position
        self.__complete = complete

    @property
    def message(self):
        """
        Get the string message of the error.

        Return type: str

        """
        return self.__message

    @property
    def position(self):
        """
        Get the position at which the error occured.

        Return type: position.Position

        """
        return self.__position

    @property
    def is_complete(self):
        """
        Query whether the error is maximal.

        Return type: bool

        """
        return self.__complete

    @property
    def errors(self):
        """
        Recursively generate a sequence of descendant errors.

        Return type: generator object

        """
        yield self
        if self.__sub_error is not None:
            yield from self.__sub_error.errors

    def __str__(self):
        return '{1}: {2}{0}'.format(self.__message, self.__position,
                                    'error: ' if self.__complete else '')

    def __repr__(self):
        return self.__str__()


class Result:

    """
    The base class of all parser result types.

    `Result` implements a discriminated union of failures and successes.
    This class exposes the following methods:

      - is_failure: query if the object represents failure
      - is_success: query if the object represents success

    These should be mutually exclusive, that is, one and only one method
    should return `True` (or equivalent). The other should return
    `False` (or equivalent). Thus, child classes must so to it that they
    only override one method to return `True`.

    """

    @property
    def is_failure(self):
        """
        Query if the object is a failure.

        Return type: bool

        """
        return False

    @property
    def is_success(self):
        """
        Query if the object is a success.

        Return type: bool

        """

        return False


class Failure(Result):

    """
    Represents a parsing failure.

    A `Failure` has an associated position, which should normally
    correspond to the index in input of the terminal symbol which
    triggered the parse failure.

    """

    def __init__(self, errors):
        self.__errors = list(errors)

    @property
    def errors(self):
        """
        Return the list of errors encapsulated by the object.

        Return type: list of ParseError

        """
        return self.__errors

    def is_failure(self):
        """Return `True` to indicate that this is a failure."""
        return True

    def __str__(self):
        return 'Failure({})'.format(self.__errors.__str__())

    def __repr__(self):
        return 'Failure({})'.format(self.__errors.__repr__())


class Success(Result):

    """
    Represents a parsing success.

    A `Success` carried a associated node, which is the syntax tree
    which the success object represents.

    """

    def __init__(self, node):
        self.node = node

    def is_success(self):
        """Return `True` to indicate that this is a success."""
        return True

    def __str__(self):
        return 'Success({})'.format(self.node.full_match.__str__())

    def __repr__(self):
        return 'Success({})'.format(self.node.full_match.__repr__())


def print_error(parse_error):
    """Print a `ParseError` to standard output."""

    indent = 0
    for error in parse_error.errors:
        print(' ' * indent, error, sep='')
        indent = 2
