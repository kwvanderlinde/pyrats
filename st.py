# -*- python3 -*-

"""
Provides syntax tree types and functions for languages.

This module does not presuppose a particular language, and it can
represent both concrete and abstract syntax trees. The following classes
are exported:

  - Node: a generic node in a syntax tree

Note that `Node` really just implements a generic labelled tree where
a tree is identified with its root node and children are ordered by the
node's construction. Thus, accessing a node in a tree is the same as
retrieving an entire subtree.

"""


class Node:

    """
    A node in the syntax tree.

    An Node has several data attached to it:

      - label: A label identifying the node or its type in some way.
      - value: A value to be stored in the node
      - children: A generator of child nodes

    For example, if we want to represent a grammar production for a
    nonterminal 'example', with child nodes 'this', 'is', 'an',
    'example', and value 'this is an example', we could write

        children = ['this', 'is', 'an', 'example']
        Node('example', ' '.join(children), children)

    In no way are `label`, `value` or `children` required to be unique,
    and mathematically we only need the `value` property, or at most
    `value` and `children`. The label exists mostly for deciding how to
    handle the node, e.g. an interpreter adds a node's children if the
    node is labelled as '+', or multiplies the children if labelled as
    '*'.

    """

    def __init__(self, label=None, value=None, children=None):
        self.__label = label
        self.__value = value
        self.__children = list(children) if children is not None else []
        for child in self.__children:
            assert isinstance(child, Node)

    @property
    def label(self):
        """
        Get the label associated with the node.

        Return type: unspecified

        """
        return self.__label

    @property
    def value(self):
        """
        Get the value associated with the node.

        Return type: unspecified

        """
        return self.__value

    @property
    def children(self):
        """
        Return the node's children.

        Return type: list of unspecified type.

        """
        return self.__children

    @property
    def is_leaf(self):
        """
        Check if the node is a leaf, that is, it has no children.

        Return type: bool

        """
        return len(self.__children) == 0

    @property
    def is_internal(self):
        """
        Check if the node is internal, that is, it is not a leaf.

        Return type: bool

        """
        return not self.is_leaf

    def _write(self, prepend=''):
        # Transform node into a list of strings, i.e. printable lines.
        if self.__value is not None:
            result = ['+-{} [{}]\n'.format(self.__label, repr(self.__value))]
        else:
            result = ['+-{}\n'.format(self.__label)]
        for child in self.children[:-1]:
            result += [prepend] + child._write(prepend + '| ')
        for child in self.children[-1:]:
            result += [prepend] + child._write(prepend + '  ')
        return result

    def __str__(self):
        return ''.join(self._write(prepend='  '))

    def __repr__(self):
        return self.__str__()
