# -*- python3 -*-
"""
Provides a parser for Parsing Expression Grammars.

This modules exports the following classes:

  - Parser: A parsing automaton for a given grammar and input.

"""

from position import Position
import expression


class Parser:

    """
    Encapsulates a parsing automaton for a grammar and given input.

    A `Parser` wraps a grammar and input from a file so that the
    contents of the file can be parsed in cooperation with the
    expression builder types. To support this cooperation, the `Parser`
    class exposes the following methods:

      - position_at_index: get the abstract position for a given index
      - at_index: get the Terminal symbol at a given index
      - sub_input: get a substring of the file's contents
      - match: attempt to recognize an expression at a given index.

    Also exposes are the following attributes:

      - grammar: The grammar from which the `Parser` was constructed.

    """

    def __init__(self, grammar, filename):
        self.grammar = grammar

        self.__filename = filename

        # Read the file into the parser, with corresponding Position instances.
        with open(filename, 'r') as file:
            self.__input_string = file.read()

        self.__input_positions = [None] * (len(self.__input_string) + 1)
        line = 1
        column = 0
        for index, char in enumerate(self.__input_string):
            self.__input_positions[index] = Position(filename, line, column)
            if char == '\n':
                line += 1
                column = 0
            else:
                column += 1
        last_position = Position(filename, line, column)
        self.__input_positions[len(self.__input_string)] = last_position

        self.__matrix = dict(
            (nonterminal, [None] * (len(self.__input_string) + 1))
            for nonterminal in self.grammar.nonterminals)

    def position_at_index(self, index):
        """
        Get the position corresponding to `index`.

        Return type: position.Position

        """

        return self.__input_positions[index]

    def at_index(self, position):
        """
        Get the Terminal symbol at `index`.

        Return type: expression.Terminal

        """

        if position >= len(self.__input_string) or position < 0:
            return None
        return expression.Terminal(self.__input_string[position])

    def sub_input(self, start, end):
        """
        Get the substring of input in the range [`start`, `end`).

        Return type: str

        """
        return self.__input_string[start: end]

    @property
    def input(self):
        """
        Get the entire input string.

        Return type: str

        """
        return self.__input_string

    def match(self, expression, index, matcher):
        """
        Attempt to recognize `expression` at `index`.

        This method memoizes its results. This method is *only* intended
        to memoize nonterminal symbols, and will raise a `KeyError` if
        another expression is passed.

        `matcher` is a callback which will be executed if `expression`
        is not already memoized. That is, `matcher` is the actual
        parsing routine.

        Return type: result.Result.

        """

        result = self.__matrix[expression][index]
        if result is not None:
            return result

        result = matcher()
        self.__matrix[expression][index] = result
        return result

    def run(self):
        """
        Attempt to recognize the grammar's start expression.

        Calling `parser.run()` is guaranteed to be equivalent to
        `parser.grammar.start.match(parser, 0)`.

        Return type: result.Result

        """
        return self.grammar.start.match(self, 0)
