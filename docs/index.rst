.. PyRats documentation master file, created by
   sphinx-quickstart on Sun Oct 13 21:37:12 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyRats's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 4

   expression
   grammar
   packrat
   position
   result
   st
   symbol

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

