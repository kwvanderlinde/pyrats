#flake8: noqa

import expression
import grammar
import parser

_my_grammar = grammar.Grammar('Grammar', [
    ('Grammar', expression.Sequence([expression.Drop('Spacing'), expression.OneOrMore('Definition'), expression.Drop('EndOfFile')])),
    ('Definition', expression.Sequence(['Identifier', expression.Drop('LEFTARROW'), 'Expression'])),
    ('Expression', expression.Sequence(['Sequence', expression.Repeat(expression.Sequence([expression.Drop('SLASH'), 'Sequence']))])),
    ('Sequence', expression.Repeat('Prefix')),
    ('Prefix', expression.Sequence([expression.Option(expression.Choose(['AND', 'NOT', 'DROP', 'HALFDROP'])), 'Suffix'])),
    ('Suffix', expression.Sequence(['Primary', expression.Option(expression.Choose(['QUESTION', 'STAR', 'PLUS']))])),
    ('Primary', expression.Choose([expression.Sequence(['Identifier', expression.Not('LEFTARROW')]), expression.Sequence([expression.Drop('OPEN'), 'Expression', expression.Drop('CLOSE')]), 'Literal', 'Class', 'DOT'])),
    ('Identifier', expression.HalfDrop(expression.Sequence(['IdentStart', expression.Repeat('IdentCont'), expression.Drop('Spacing')]))),
    ('IdentStart', expression.Class('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_')),
    ('IdentCont', expression.Choose(['IdentStart', expression.Class('0123456789')])),
    ('Literal', expression.Choose([expression.Sequence([expression.Drop(expression.Class("'")), expression.Repeat(expression.Sequence([expression.Not(expression.Class("'")), 'LitChar'])), expression.Drop(expression.Class("'")), expression.Drop('Spacing')]), expression.Sequence([expression.Drop(expression.Class('"')), expression.Repeat(expression.Sequence([expression.Not(expression.Class('"')), 'LitChar'])), expression.Drop(expression.Class('"')), expression.Drop('Spacing')])])),
    ('LitChar', expression.Choose([expression.Sequence([expression.Literal('\\'), expression.Class('fnrt\'"[]\\')]), expression.Sequence([expression.Literal('\\'), expression.Class('012'), expression.Class('01234567'), expression.Class('01234567')]), expression.Sequence([expression.Literal('\\'), expression.Class('01234567'), expression.Option(expression.Class('01234567'))]), expression.Sequence([expression.Not(expression.Literal('\\')), expression.Dot()])])),
    ('Class', expression.Sequence([expression.Drop(expression.Literal('[')), expression.Repeat(expression.Sequence([expression.Not(expression.Literal(']')), 'Range'])), expression.Drop(expression.Literal(']')), expression.Drop('Spacing')])),
    ('Range', expression.Choose([expression.Sequence(['Char', expression.Drop(expression.Literal('-')), 'Char']), 'Char'])),
    ('Char', expression.Choose([expression.Sequence([expression.Literal('\\'), expression.Class('fnrt\'"[]\\-')]), expression.Sequence([expression.Literal('\\'), expression.Class('012'), expression.Class('01234567'), expression.Class('01234567')]), expression.Sequence([expression.Literal('\\'), expression.Class('01234567'), expression.Option(expression.Class('01234567'))]), expression.Sequence([expression.Not(expression.Class('-]\\')), expression.Dot()])])),
    ('LEFTARROW', expression.Sequence([expression.Literal('<-'), expression.Drop('Spacing')])),
    ('SLASH', expression.Sequence([expression.Literal('/'), expression.Drop('Spacing')])),
    ('AND', expression.Sequence([expression.Literal('&'), expression.Drop('Spacing')])),
    ('NOT', expression.Sequence([expression.Literal('!'), expression.Drop('Spacing')])),
    ('DROP', expression.Sequence([expression.Literal(':'), expression.Drop('Spacing')])),
    ('HALFDROP', expression.Sequence([expression.Literal(';'), expression.Drop('Spacing')])),
    ('QUESTION', expression.Sequence([expression.Literal('?'), expression.Drop('Spacing')])),
    ('STAR', expression.Sequence([expression.Literal('*'), expression.Drop('Spacing')])),
    ('PLUS', expression.Sequence([expression.Literal('+'), expression.Drop('Spacing')])),
    ('OPEN', expression.Sequence([expression.Literal('('), expression.Drop('Spacing')])),
    ('CLOSE', expression.Sequence([expression.Literal(')'), expression.Drop('Spacing')])),
    ('DOT', expression.Sequence([expression.Literal('.'), expression.Drop('Spacing')])),
    ('Spacing', expression.Repeat(expression.Choose(['Space', 'Comment']))),
    ('Comment', expression.Sequence([expression.Literal('#'), expression.Repeat(expression.Sequence([expression.Not('EndOfLine'), expression.Dot()])), expression.Drop('EndOfLine')])),
    ('Space', expression.Choose([expression.Literal(' '), expression.Literal('\t'), 'EndOfLine'])),
    ('EndOfLine', expression.Choose([expression.Literal('\r\n'), expression.Literal('\n'), expression.Literal('\r')])),
    ('EndOfFile', expression.Not(expression.Dot()))
])

def parse(filename):
    my_parser = parser.Parser(_my_grammar, filename)
    return my_parser.run()
