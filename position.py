# -*- python3 -*-

"""
Provides a class to represent an arbitrary position in a given input.

This modules exports the following classes:

  - Position: a generic position.

"""


class Position:

    """
    Represents a generic position in an input.

    A `Position` is an aggregation of common position data. It includes
    a filename, as well as a line and column number indication the
    position of a syntactic element or error.

    """

    def __init__(self, filename, line, column):
        """Initialize a `Position` with the specified data."""
        self.__filename = filename
        self.__line = line
        self.__column = column

    @property
    def filename(self):
        """
        Retrieve the filename specified for the position.

        Return type: str

        """
        return self.__filename

    @property
    def line(self):
        """
        Retrieve the line number of the position.

        Return type: int

        """
        return self.__line

    @property
    def column(self):
        """
        Retrieve the column number of the position.

        Return type: int

        """
        return self.__column

    def __str__(self):
        return '{}:{}:{}'.format(self.__filename, self.__line, self.__column)

    def __repr__(self):
        return self.__str__()
