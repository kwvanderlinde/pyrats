"""
Exports classes for building parsing expression grammars (PEG).

Together with the ``symbol`` and ``expression`` modules, defines classes
needed to build a parsing expression grammar (PEG). A PEG is defined by
Ford as a tuple :math:`G = (V_N, V_T, R, e_s)` where:

  - :math:`V_N` is a finite set of nonterminal symbols
  - :math:`V_T` is a finite set of terminal symbols
  - :math:`R` is a finite set of rules
  - :math:`e_s` is a distinguised "start expression"

The sets :math:`V_N` and :math:`V_T` must be disjoint, and the set
:math:`R` is a set of pairs :math:`(A, e)` where :math:`A` is a
nonterminal and :math:`e` is a parsing expression.

Representations of :math:`V_N` and :math:`V_T` are handled by the
:code:`symbol` module as classes :code:`symbol.Nonterminal` and
:code:`symbol.Terminal`, respectively. Parsing expressions are
represented by :code:`expression.Expression`. :math:`R` is represented
as a dictionary from :code:`symbol.Nonterminal` to
:code:`expression.Expression`. These types are aggregated into a single
:code:`Grammar` class, representing the tuple :math:`G`.

This module exports the following classes

  - Grammar Represents a PEG.

"""

import expression


class Grammar:

    """
    Represents a parsing expression grammar.

    The following methods are public:

      - start: the grammar's start symbol
      - nonterminals: an enumeration over the nonterminals in the grammar
      - rules: an enumeration over all rules in the grammar.
      - __getitem__: looks up a parsing expression which is the right
        hand side of a rule whose left hand side is the given argument.

    """

    # rules should be a list of pairs (Nonterminal, expression)
    def __init__(self, start, rules):
        # TODO: add grammar verification (i.e. nonterminals not found on
        # the right hand side of a rule; possibly also unused rules).
        assert (isinstance(start, str) or isinstance(start, expression.Base))

        if isinstance(start, str):
            self.start = expression.Nonterminal(start)
        else:
            self.start = start

        self.map = dict()
        for (lhs, expr) in rules:
            assert (isinstance(lhs, str) or lhs.is_nonterminal())
            # For convenience, we allow the user to provide string
            # values in places of expression.Nonterminals. We need to
            # check both the LHS and RHS to see if they are strings. We
            # need not do a recursive check in the RHS since the
            # compound expression builders will do this already.
            if isinstance(lhs, str):
                lhs = expression.Nonterminal(lhs)
            if isinstance(expr, str):
                expr = expression.Nonterminal(expr)

            if lhs.name in self.map:
                raise RuntimeError('Multiple rules for {0}', lhs)
            else:
                self.map[lhs.name] = expr

    @property
    def nonterminals(self):
        """Yield all nonterminal in the grammar."""
        for symbol in self.map:
            yield expression.Nonterminal(symbol)

    def __getitem__(self, key):
        assert (isinstance(key, expression.Nonterminal) or
                isinstance(key, str)), \
            '{}: {}'.format(type(key), key)
        assert key.name in self.map, '{}'.format(key)
        return self.map[key.name]
