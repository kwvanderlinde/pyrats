import expression
import grammar
import parser
import string

dot = expression.Choice(expression.Terminal(string.printable[0]), expression.Terminal(string.printable[1]))
for char in string.printable[2:]:
    dot = expression.Choice(expression.Terminal(char), dot)

rules = [(expression.Nonterminal('.'), dot)]
G = grammar.Grammar(expression.Nonterminal('.'), rules)
my_parser = parser.Parser(G, 'test files/grammar_a_test')

result = my_parser.match(G.start, 0)
