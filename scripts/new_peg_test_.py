import expression
from expression import Terminal, Nonterminal
import string
import grammar
import parser
import result

import sys
my_grammar =grammar.Grammar('Grammar', [
    ('Grammar', expression.Sequence(['Spacing', expression.OneOrMore('Definition'), 'EndOfFile'])),
    ('Definition', expression.Sequence(['Identifier', 'LEFTARROW', 'Expression'])),
    ('Expression', expression.Sequence(['Sequence', expression.Repeat((expression.Sequence(['SLASH', 'Sequence'])))])),
    ('Sequence', expression.Repeat('Prefix')),
    ('Prefix', expression.Sequence([expression.Option((expression.Choose(['AND', 'NOT']))), 'Suffix'])),
    ('Suffix', expression.Sequence(['Primary', expression.Option((expression.Choose(['QUESTION', 'STAR', 'PLUS'])))])),
    ('Primary', expression.Choose([expression.Sequence(['Identifier', expression.Not('LEFTARROW')]), expression.Sequence(['OPEN', 'Expression', 'CLOSE']), 'Literal', 'Class', 'DOT'])),
    ('Identifier', expression.Sequence(['IdentStart', expression.Repeat('IdentCont'), 'Spacing'])),
    ('IdentStart', expression.Class('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_')),
    ('IdentCont', expression.Choose(['IdentStart', expression.Class('0123456789')])),
    ('Literal', expression.Choose([expression.Sequence([expression.Class("'"), expression.Repeat((expression.Sequence([expression.Not(expression.Class("'")), 'Char']))), expression.Class("'"), 'Spacing']), expression.Sequence([expression.Class('"'), expression.Repeat((expression.Sequence([expression.Not(expression.Class('"')), 'Char']))), expression.Class('"'), 'Spacing'])])),
    ('Class', expression.Sequence([expression.Literal('['), expression.Repeat((expression.Sequence([expression.Not(expression.Literal(']')), 'Range']))), expression.Literal(']'), 'Spacing'])),
    ('Range', expression.Choose([expression.Sequence(['Char', expression.Literal('-'), 'Char']), 'Char'])),
    ('Char', expression.Choose([expression.Sequence([expression.Literal('\\'), expression.Class('nrt\'"[]\\')]), expression.Sequence([expression.Literal('\\'), expression.Class('012'), expression.Class('01234567'), expression.Class('01234567')]), expression.Sequence([expression.Literal('\\'), expression.Class('01234567'), expression.Option(expression.Class('01234567'))]), expression.Sequence([expression.Not(expression.Literal('\\')), expression.Dot()])])),
    ('LEFTARROW', expression.Sequence([expression.Literal('<-'), 'Spacing'])),
    ('SLASH', expression.Sequence([expression.Literal('/'), 'Spacing'])),
    ('AND', expression.Sequence([expression.Literal('&'), 'Spacing'])),
    ('NOT', expression.Sequence([expression.Literal('!'), 'Spacing'])),
    ('QUESTION', expression.Sequence([expression.Literal('?'), 'Spacing'])),
    ('STAR', expression.Sequence([expression.Literal('*'), 'Spacing'])),
    ('PLUS', expression.Sequence([expression.Literal('+'), 'Spacing'])),
    ('OPEN', expression.Sequence([expression.Literal('('), 'Spacing'])),
    ('CLOSE', expression.Sequence([expression.Literal(')'), 'Spacing'])),
    ('DOT', expression.Sequence([expression.Literal('.'), 'Spacing'])),
    ('Spacing', expression.Repeat((expression.Choose(['Space', 'Comment'])))),
    ('Comment', expression.Sequence([expression.Literal('#'), expression.Repeat((expression.Sequence([expression.Not('EndOfLine'), expression.Dot()]))), 'EndOfLine'])),
    ('Space', expression.Choose([expression.Literal(' '), expression.Literal('\t'), 'EndOfLine'])),
    ('EndOfLine', expression.Choose([expression.Literal('\r\n'), expression.Literal('\n'), expression.Literal('\r')])),
    ('EndOfFile', expression.Not(expression.Dot()))
])
my_parser = parser.Parser(my_grammar, 'test files/peg_test')
result = my_parser.match(my_parser.grammar.start, 0)
if result.is_failure:
    result.print_error(result)
    sys.exit(1)
else:
    print(result.node)
