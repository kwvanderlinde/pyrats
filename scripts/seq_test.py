import expression
from expression import Terminal, Nonterminal
import string
import grammar
import parser

Char = Nonterminal('Grammar')

rules = [
    (Char, expression.sequence([
        expression.dot(),
        expression.choice([
            expression.sequence([Terminal('h'), Terminal('i')]),
            expression.sequence([expression.dot(), expression.dot(), expression.dot()])
        ]),
        expression.dot()])
    )
]

my_grammar = grammar.Grammar(Char, rules)

my_parser = parser.Parser(my_grammar, 'test files/arithmetic')
my_result = my_parser.match(my_parser.grammar.start, 0)
