import expression
from expression import Nonterminal, Terminal
import grammar
import parser

import string

Module = Nonterminal('Module')
Decl = Nonterminal('Decl')
VarDecl = Nonterminal('VarDecl')
Expression = Nonterminal('Expression')

Spacing = Nonterminal('Spacing')
Space = Nonterminal('Space')
EndOfLine = Nonterminal('EndOfLine')
EndOfFile = Nonterminal('EndOfFile')

Identifier = Nonterminal('Identifier')
IdentStart = Nonterminal('IdentStart')
IdentCont = Nonterminal('IdentCont')

rules = [
    (Module, expression.Repetition(expression.sequence([Decl, expression.option(Spacing)]))),
    (Decl, VarDecl),
    (VarDecl, expression.sequence([
        expression.literal('def'),
        Spacing,
        Identifier,
        expression.option(Spacing),
        expression.literal('='),
        expression.option(Spacing),
        Expression,
        expression.option(Spacing),
        expression.literal(';')
        ])),
    (Expression, expression.sequence([
        expression.literal('new'),
        Spacing,
        Identifier,
        expression.option(Spacing),
        expression.literal('('),
        expression.option(Spacing),
        expression.literal(')')
        ])),

    (Identifier, expression.Drop(
        expression.sequence([IdentStart, expression.Repetition(IdentCont), expression.Drop(Spacing)]))),
    (IdentStart, expression.char_class(string.ascii_letters + '_')),
    (IdentCont, expression.choice([IdentStart, expression.char_class(string.digits)])),

    (Spacing, expression.Repetition(Space)),
    (Space, expression.choice([Terminal(' '), Terminal('\t'), expression.Drop(EndOfLine)])),
    (EndOfLine, expression.choice([expression.literal('\r\n'), Terminal('\n'), Terminal('\r')])),
    (EndOfFile, expression.Drop(expression.Not(expression.dot())))
]

my_grammar = grammar.Grammar(expression.sequence([Module, EndOfFile]), rules)
my_parser = parser.Parser(my_grammar, 'test files/c_test')
my_result = my_parser.match(my_parser.grammar.start, 0)
