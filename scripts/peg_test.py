import expression
from expression import Terminal, Nonterminal
import string
import grammar
import parser

# Hierarchical syntax
Grammar = Nonterminal('Grammar')
Definition = Nonterminal('Definition')

Expression = Nonterminal('Expression')
Sequence = Nonterminal('Sequence')
Prefix = Nonterminal('Prefix')
Suffix = Nonterminal('Suffix')
Primary = Nonterminal('Primary')

# Lexical syntax
Identifier = Nonterminal('Identifier')
IdentStart = Nonterminal('IdentStart')
IdentCont = Nonterminal('IdentCont')

Literal = Nonterminal('Literal')
Class = Nonterminal('Class')
Range = Nonterminal('Range')
Char = Nonterminal('Char')

LEFTARROW = Nonterminal('LEFTARROW')
SLASH = Nonterminal('SLASH')
AND = Nonterminal('AND')
NOT = Nonterminal('NOT')
QUESTION = Nonterminal('QUESTION')
STAR = Nonterminal('STAR')
PLUS = Nonterminal('PLUS')
OPEN = Nonterminal('OPEN')
CLOSE = Nonterminal('CLOSE')
DOT = Nonterminal('DOT')

Spacing = Nonterminal('Spacing')
Comment = Nonterminal('Comment')
Space = Nonterminal('Space')
EndOfLine = Nonterminal('EndOfLine')
EndOfFile = Nonterminal('EndOfFile')

rules = [
    ('Grammar', expression.Sequence([expression.Drop(Spacing), expression.OneOrMore(Definition), expression.Drop(EndOfFile)])),
    ('Definition', expression.Sequence([Identifier, expression.Drop(LEFTARROW), Expression])),
    ('Expression', expression.Sequence([Sequence, expression.Repeat(expression.Sequence([expression.Drop(SLASH), Sequence]))])),
    ('Sequence', expression.Repeat(Prefix)),
    ('Prefix', expression.Sequence([expression.Option(expression.Choose([AND, NOT])), Suffix])),
    ('Suffix', expression.Sequence([Primary, expression.Option(expression.Choose([QUESTION, STAR, PLUS]))])),
    ('Primary', expression.Choose([
        expression.Sequence([Identifier, expression.Drop(expression.Not(LEFTARROW))]),
        expression.Sequence([expression.Drop(OPEN), Expression, expression.Drop(CLOSE)]),
        Literal,
        Class,
        DOT
        ])),

    ('Identifier',
        expression.Sequence([IdentStart, expression.Repeat(IdentCont), expression.Drop(Spacing)])),
    ('IdentStart', expression.Class(string.ascii_letters + '_')),
    ('IdentCont', expression.Choose([IdentStart, expression.Class(string.digits)])),
    ('Literal', expression.Choose([
        expression.Sequence([expression.Drop(Terminal('\'')), expression.Repeat(expression.Sequence([expression.Drop(expression.Not(Terminal('\''))), Char])), expression.Drop(Terminal('\'')), expression.Drop(Spacing)]),
        expression.Sequence([expression.Drop(Terminal('"')), expression.Repeat(expression.Sequence([expression.Drop(expression.Not(Terminal('"'))), Char])), expression.Drop(Terminal('"')), expression.Drop(Spacing)])
        ])),
    ('Class', expression.Sequence([expression.Drop(Terminal('[')), expression.Repeat(expression.Sequence([expression.Drop(expression.Not(Terminal(']'))), Range])), expression.Drop(Terminal(']')), expression.Drop(Spacing)])),
    ('Range', expression.Choose([
        expression.Sequence([Char, expression.Drop(Terminal('-')), Char]),
        Char])),
    ('Char', expression.Choose([
        expression.Sequence([Terminal('\\'), expression.Class('nrt\'"[]\\')]),
        expression.Sequence([Terminal('\\'), expression.Class('012'), expression.Class('01234567'), expression.Class('01234567')]),
        expression.Sequence([Terminal('\\'), expression.Class('01234567'), expression.Option(expression.Class('01234567'))]),
        expression.Sequence([expression.Drop(expression.Not(Terminal('\\'))), expression.Dot()])
        ])),
    ('LEFTARROW', expression.Sequence([expression.Literal('<-'), expression.Drop(Spacing)])),
    ('SLASH', expression.Sequence([Terminal('/'), expression.Drop(Spacing)])),
    ('AND', expression.Sequence([Terminal('&'), expression.Drop(Spacing)])),
    ('NOT', expression.Sequence([Terminal('!'), expression.Drop(Spacing)])),
    ('QUESTION', expression.Sequence([Terminal('?'), expression.Drop(Spacing)])),
    ('STAR', expression.Sequence([Terminal('*'), expression.Drop(Spacing)])),
    ('PLUS', expression.Sequence([Terminal('+'), expression.Drop(Spacing)])),
    ('OPEN', expression.Sequence([Terminal('('), expression.Drop(Spacing)])),
    ('CLOSE', expression.Sequence([Terminal(')'), expression.Drop(Spacing)])),
    ('DOT', expression.Sequence([Terminal('.'), expression.Drop(Spacing)])),

    ('Spacing', expression.Repeat(expression.Choose([Space, Comment]))),
    ('Comment', expression.Sequence([Terminal('#'), expression.Repeat(expression.Sequence([expression.Drop(expression.Not(EndOfLine)), expression.Dot()])), expression.Drop(EndOfLine)])),
    ('Space', expression.Choose([Terminal(' '), Terminal('\t'), expression.Drop(EndOfLine)])),
    ('EndOfLine', expression.Choose([expression.Literal('\r\n'), Terminal('\n'), Terminal('\r')])),
    ('EndOfFile', expression.Drop(expression.Not(expression.Dot())))
]

my_grammar = grammar.Grammar(Grammar, rules)
my_parser = parser.Parser(my_grammar, 'test files/peg_test')
result = my_parser.match(my_parser.grammar.start, 0)

my_parser2 = parser.Parser(my_grammar, 'test files/arithmetic')
result2 = my_parser2.match(my_parser2.grammar.start, 0)
