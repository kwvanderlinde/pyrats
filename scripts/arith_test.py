import expression
from expression import Nonterminal, Terminal
import grammar
import parser

import string

Term = Nonterminal('Term')
Add = Nonterminal('Add')
Sub = Nonterminal('Sub')
Factor = Nonterminal('Factor')
Mul = Nonterminal('Mul')
Div = Nonterminal('Div')
Primary = Nonterminal('Primary')
Parens = Nonterminal('Parens')
Neg = Nonterminal('Neg')
Pos = Nonterminal('Pos')
Number = Nonterminal('Number')
Variable = Nonterminal('Variable')

Identifier = Nonterminal('Identifier')
IdentStart = Nonterminal('IdentStart')
IdentCont = Nonterminal('IdentCont')

#Spacing = Nonterminal('Spacing')
Spacing = expression.Drop(Nonterminal('Spacing'))
Space = Nonterminal('Space')
EndOfLine = Nonterminal('EndOfLine')

def literal(chars):
    return expression.Drop(expression.literal(chars))

# TODO: Add explicit spacing
rules = [
    (Term, expression.sequence([Factor, Spacing, expression.Repetition(expression.choice([Add, Sub]))])),
    (Add, expression.sequence([literal('+'), Spacing, Factor])),
    (Sub, expression.sequence([literal('-'), Spacing, Factor])),
    (Factor, expression.sequence([Primary, Spacing, expression.Repetition(expression.choice([Mul, Div]))])),
    (Mul, expression.sequence([literal('*'), Spacing, Primary])),
    (Div, expression.sequence([literal('/'), Spacing, Primary])),
    (Primary, expression.choice([Parens, Neg, Pos, Number, Variable])),
    (Parens, expression.sequence([Spacing, literal('('), Spacing, Term, Spacing, literal(')'), Spacing])),
    (Neg, expression.sequence([literal('-'), Spacing, Primary])),
    (Pos, expression.sequence([literal('+'), Spacing, Primary])),
    (Number, expression.one_or_more(expression.Drop(expression.char_class(string.digits)))),

    (Variable, expression.Drop(Identifier)),

    (Identifier, expression.sequence([IdentStart, expression.Repetition(IdentCont)])),
    (IdentStart, expression.Drop(expression.char_class(string.ascii_letters))),
    (IdentCont, expression.choice([IdentStart, expression.Drop(expression.char_class(string.digits))])),

    (Nonterminal('Spacing'), expression.Repetition(Space)),
    (Space, expression.choice([literal(' '), literal('\t'), EndOfLine])),
    (EndOfLine, expression.choice([literal('\r\n'), literal('\n'), literal('\r')]))
]

my_grammar = grammar.Grammar(start=Term, rules=rules)
my_parser = parser.Parser(grammar=my_grammar, filename='test files/arithmetic_test')
my_result = my_parser.match(my_grammar.start, 0)
