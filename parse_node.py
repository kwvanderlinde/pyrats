"""
Provides a syntax tree type specialized for use in PyRats.

This module is built specifically for use in PyRats. The exported
definitions have been selected because they are more useful for
implementing PyRats then the generic st module.

The following types are exported:

  - ParseNode: a generic node in a syntax tree

"""


class ParseNode:

    """
    A node in a syntax tree.

    A ParseNode has several attributes:

      - expression: the expression which produced the node;
      - full_match: all the input text consumed by the expression;
      - minimal_match: the minimal contiguous cover of the children; and
      - children: a list of child nodes.

    """

    def __init__(self, expression, input_string, start, end, sub_matches,
                 children):
        """
        Initialize a ParseNode.

        Parameters `input_string`, `start` and `end` together specify
        the node's `full_match` attribute. Specifically `full_match`
        will return the same as `input_string[start: end]`.
        `minimal_match` is determined by the smallest range `[a: b]`
        containing the `[start: end]` ranges it's children were
        constructed with.

        """

        self.__expression = expression
        self.__input = input_string
        self.__start = start
        self.__end = end
        self.__sub_matches = list(sub_matches)
        self.__children = list(children)

    @property
    def expression(self):
        """
        Get the expression corresponding to the node.

        Return type: expression.Base

        """
        return self.__expression

    @property
    def start(self):
        """
        Get the start index of the match in the input string.

        Return type: int

        """
        return self.__start

    @property
    def end(self):
        """
        Get the end index of the match in the input string.

        Return type: int

        """
        return self.__end

    @property
    def full_match(self):
        """
        Get the entire match.

        Return type: str

        """
        return self.__input[self.__start: self.__end]

    @property
    def sub_matches(self):
        """
        Yield all submatches.

        Return type: list of str

        """
        return self.__sub_matches

    @property
    def children(self):
        """
        Get a list of the node's children.

        Return type: list of ParseNode

        """
        return self.__children

    def flatten(self):
        """
        Attempt to flatten the node.

        Flattening a node results in a simplified parse tree.
        Specifically, unimportant nodes relating to PEG reductions are
        eliminated, being replacing by a logical equivalent.

        Return type: list of ParseNode

        """

        return self.__expression.flatten(self)

    def _write(self, prepend=''):
        # Transform node into a list of strings, i.e. printable lines.
        result = ['+-{} {}\n'.format(self.__expression, self.sub_matches)]
        for child in self.children[:-1]:
            result += [prepend] + child._write(prepend + '| ')
        for child in self.children[-1:]:
            result += [prepend] + child._write(prepend + '  ')
        return result

    def __str__(self):
        return ''.join(self._write(prepend='  '))

    def __repr__(self):
        return self.__str__()
