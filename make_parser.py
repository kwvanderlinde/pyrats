# flake8: noqa

import peg
import printer
import result

import sys

if __name__ == "__main__":
    assert len(sys.argv) > 1
    grammar_file = sys.argv[1]

    grammar_result = peg.parse(grammar_file)
    if grammar_result.is_failure:
        grammar_result
        result.print_error(grammar_result)
        exit(1)

    print('import expression')
    print('import grammar')
    print('import parser')
    print('')

    print('_my_grammar = ', end='')
    print(printer.pretty_print(grammar_result.node))

    print('')
    print('def parse(filename):')
    print('    my_parser = parser.Parser(_my_grammar, filename)')
    print('    return my_parser.run()')

